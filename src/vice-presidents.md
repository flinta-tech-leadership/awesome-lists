---
layout: layouts/default.njk
title: Vice Presidents Engineering
---
# FLINTA Vice Presidents Engineering
Everyone who identifies themselves as a gender, non-binary, non-male, women,  ... ❤️

## Adding Rules
- add by country
- add links to the profile
- if you add someone else as yourself, please ask the person before you do
- please sort alphabetically (countries and names by first name)

## Countries

## Colorado
- [Kathy Keating](https://twitter.com/kathkeating) VP Engineering [@AdHocTeam](https://twitter.com/AdHocTeam) | CTO Advisor | Mentor #GiveFirst
